# Setup
 - copy .env.example to .env 
 - use makefile or docker-compose to start the container stack 

   
        make start
or

        docker-compose up -d --build --force-recreate

 - browse to http://localhost:8080/web_app/new.php


# History
You are a software tester responsible for the QA for a company (AwesomeCorp) that, like any good company, records the 
details of their employees. AwesomeCorp has developed an internal system to keep track of their employees. 

This system was originally built in 2007 and has gone through many iterations and different developers over the years. 

Originally, in 2007 employees could "self-register" via a web UI when they joined AwesomeCorp.

 - Accessible via `/web_app/new.php`
 - This allows the employee to enter their details, and choose their own password.
 - The employee gets re-direct the employee to a dashboard page once they have registered. `/web_app/dashboard.php`


# Scenario
Like any company - AwesomeCorp's code has some problems! - as the software tester, it is your job to find them. 
You will create a test plan for this feature.
It will also be your job to write automation tests to ensure these problems can't come back in the future.  


## The Rules
 - Please limit your time on this to two hours or less.
 - Please fork this repository, and create a Pull Request into your forked repo, send us the link to the PR.
 - Any automated tests should be saved into the test directory of the repository, and update the readme.md file with instructions to run them
 - Feel free to provide diagrams or any documentation, on the PR.
 - **We do not mind if you run out of time, or cannot find all the problems, or complete automated tests**. However, we expect you to tell us:
     - How you _would_ have liked to write the tests.
     - What challenges you faced when deciding on a solution.
     - Any additional information you would like to know to do a better job.
    

# Tech Stuff
- Docker Compose is required to run the container,  
- You can find the database schema in `assets/db.sql` or by connecting to mysql on localhost using credentials in .env.example