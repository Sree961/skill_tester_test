# Tests

##Test Scenarios
Identified test scenarios :-
![img.png](img.png)

## Automation Test details:-
Created a stand-alone java selenium project , so add the all dependent jars from resource folders.
Refer the test.java (positive)
negative.java (negative) -> Blank values to few fields are also processing.

There is no external reports used, it is on pure testng.

##Issue faced:-
Unable to validate the entered data. Getting blank value for getText() methods.
Unable to validate the password field in database because it is in encrypted format.

###Note:-
Used my existing code for automation.

