package Test;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class webDriverInit {

    public WebDriver getDriver(String URL) {
        System.setProperty("webdriver.chrome.driver", "resources/WebDriver/chromedriver.exe");
        WebDriver driver=new ChromeDriver();
        driver.get(URL);
        driver.manage().window().maximize();
        return driver;
    }
}
