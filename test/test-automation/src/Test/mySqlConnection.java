package Test;

import org.testng.Assert;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class mySqlConnection {

    String username="skilltester", password="password", databaseName="skill_tester_test", databseHostName="127.0.0.1";

    public boolean validate(String name, String phonenumber,  String email, String empType ) {
        boolean flag=false;
        try{
            Class.forName("com.mysql.jdbc.Driver");
            Connection con= DriverManager.getConnection(
                    "jdbc:mysql://"+ databseHostName+":3306/" + databaseName,username,password);
//here sonoo is database name, root is username and password
            Statement stmt=con.createStatement();
            ResultSet rs=stmt.executeQuery("select id, name, type, email, phone_number from employee");

            while(rs.next())
                if(rs.getString(2).equals(name) &&
                rs.getString(4).equals(email) &&
                rs.getString(5).equals(phonenumber)) {
                    if(empType.toLowerCase().startsWith("part")) {
                        if(rs.getString(3).equals("1")) {
                            flag=true;
                        }
                    }else {
                        if(rs.getString(3).equals("2")) {
                            flag=true;
                        }
                    }
                }
            con.close();
            if (! flag) {
                Assert.fail("Match is not found in DB");
            }
        }catch(Exception e){ System.out.println(e);}

        return flag;
    }

}

