package Test;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import poj.Dashboard;
import poj.New;

import java.io.File;
import java.io.IOException;
import java.util.*;

public class test {

    @DataProvider(name = "data-provider")
    private Iterator<Object[]> getTestCaseDetails() throws IOException {
        String fileName="resources/TestData/testdata.csv";
        Collection<Object[]> dp = readCSV_CollectorMap(fileName);
        return dp.iterator();
    }

    String URL="http://localhost:8080/web_app/new.php";
    @Test (dataProvider = "data-provider")
    public void myTest (Map map) {
        WebDriver driver = new webDriverInit().getDriver(URL);
        String name=map.get("Name").toString();
        String phonenumber=map.get("PhoneNumber").toString();
        String password=map.get("Password").toString();
        String email=map.get("EmailID").toString();
        String empType=map.get("Type").toString();

        new New().enterDetails(driver, name, phonenumber, password, email, empType);
       //new New().validateDetails(driver, name, phonenumber, password, email, empType);
        new New().submit(driver);
        new Dashboard().validate(driver);
         new mySqlConnection().validate(name, phonenumber, email, empType);
         driver.quit();

    }

    public static Collection<Object[]> readCSV_CollectorMap(String fileName) throws IOException {
        File csvFile = new File(fileName);
        CsvMapper mapper = new CsvMapper();
        CsvSchema schema = CsvSchema.emptySchema().withHeader(); // use first row as header; otherwise defaults are fine
        MappingIterator<Map<String,String>> it = mapper.readerFor(Map.class)
                .with(schema)
                .readValues(csvFile);
        Collection<Object[]> dp = new ArrayList<Object[]>();
        for(Map<String,String> map:it.readAll()){
            dp.add(new Object[]{map});
        }
        return dp;
    }

}
