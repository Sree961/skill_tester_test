package poj;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import java.util.List;

public class New {
    static By header = By.tagName("h1");
    static By emp_Name = By.name("name");
    static By emp_PhoneNumber= By.name("phone_number");
    static By emp_Password=By.name("password");
    static By emp_email=By.name("email");
    static By emp_employeeType=By.name("employee_type");
    static By create_button = By.xpath("//form/input");

    public boolean enterDetails(WebDriver driver, String name, String phonenumber, String password, String email, String empType) {
        if(driver.findElement(emp_Name).isEnabled()) {
            driver.findElement(emp_Name).sendKeys(name);
        } else {
            Assert.fail("Employee Name field is disabled.");
            return false;
        }

        if(driver.findElement(emp_PhoneNumber).isEnabled()) {
            driver.findElement(emp_PhoneNumber).sendKeys(phonenumber);
        } else {
            Assert.fail("Employee phone number field is disabled.");
            return false;
        }

        if(driver.findElement(emp_Password).isEnabled()) {
            driver.findElement(emp_Password).sendKeys(password);
        } else {
            Assert.fail("Employee Password field is disabled.");
            return false;
        }

        if(driver.findElement(emp_email).isEnabled()) {
            driver.findElement(emp_email).sendKeys(email);
        } else {
            Assert.fail("Employee email field is disabled.");
            return false;
        }

        if(driver.findElement(emp_employeeType).isEnabled()) {
            WebElement eType= driver.findElement(emp_employeeType);
            Select select= new Select(eType);
            select.selectByVisibleText(empType);
        } else {
            Assert.fail("Employee Type field is disabled.");
            return false;
        }
        return true;
    }

    public boolean validateDetails(WebDriver driver, String name, String phonenumber, String password, String email, String empType) {
        if(driver.findElement(emp_Name).isEnabled()) {
            if (! name.equals(driver.findElement(emp_Name).getText())) {
                Assert.fail("Employee Name value is not matched.");
            }
        } else {
            Assert.fail("Employee Name field is disabled.");
            return false;
        }

        if(driver.findElement(emp_PhoneNumber).isEnabled()) {
            if (! phonenumber.equals(driver.findElement(emp_PhoneNumber).getText())) {
                Assert.fail("Employee phonenumber value is not matched.");
            }
        } else {
            Assert.fail("Employee phone number field is disabled.");
            return false;
        }

        if(driver.findElement(emp_Password).isEnabled()) {
            if (! password.equals(driver.findElement(emp_Password).getText())) {
                Assert.fail("Employee password value is not matched.");
            }
        } else {
            Assert.fail("Employee Password field is disabled.");
            return false;
        }

        if(driver.findElement(emp_email).isEnabled()) {
            if (! email.equals(driver.findElement(emp_email).getText())) {
                Assert.fail("Employee email value is not matched.");
            }
        } else {
            Assert.fail("Employee email field is disabled.");
            return false;
        }

        if(driver.findElement(emp_employeeType).isEnabled()) {
            WebElement eType= driver.findElement(emp_employeeType);
            Select select= new Select(eType);
            List s=select.getAllSelectedOptions();
            boolean flag=false;
            for(Object s1:s ) {
                if(s1.toString().equals(emp_employeeType)) {
                    flag=true;
                    break;
                }
            }
            if (! flag) {
                Assert.fail("Employee type value is not matched.");
            }
        } else {
            Assert.fail("Employee Type field is disabled.");
            return false;
        }
        return true;
    }

    public boolean submit(WebDriver driver) {
        if(driver.findElement(create_button).isEnabled()) {
            driver.findElement(create_button).click();
        } else {
            Assert.fail("Create Button is disabled.");
            return false;
        }
        return true;
    }
}
