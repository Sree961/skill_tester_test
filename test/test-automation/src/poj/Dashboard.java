package poj;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class Dashboard {
    private static By dashboard = By.tagName("h1");

    public void validate(WebDriver webDriver) {
        if(! webDriver.findElement(dashboard).getText().equals("Dashboard!")) {
            Assert.fail("Dashboard page is not displayed.");
        }
    }
}
