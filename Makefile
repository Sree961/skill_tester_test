.PHONY: start stop restart exec

start:
	docker-compose up -d --build --force-recreate

stop:
	docker-compose down -v

restart: stop start

exec:
	docker-compose exec app bash